FROM node:10
WORKDIR /usr/src/myapp

COPY package.json yarn.lock ./
RUN yarn

COPY . .
EXPOSE 8080
CMD [ "npm", "start" ]