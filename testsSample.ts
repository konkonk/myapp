// eligibility.test.ts

import sinon from 'sinon';
import { eligibility } from '../../dist/controllers/eligibility';
import * as camunda from '../../dist/core/camunda';
import * as eligibilityQuery from '../../dist/database/models/eligibility';

import { camundaResponse, VariablesPayload } from '../../src/core/camunda';

const gb = require('../../dist/core/globals');
jest.mock('../../dist/core/globals');
const db = require('../../dist/database/connection');
jest.mock('../../dist/database/connection');

const req: any = 
    {
        correlationId: 'sss', 
        body: {
            client_id: 'kostya',
        }
    };

const res: any = { ok: true };

type TParentModelPayload = {
    withVariablesInReturn: boolean,
    businessKey?: string,
    variables: VariablesPayload,
};

const camundaNormalizedPayload: VariablesPayload = {
    suspended_flag:
    {
        type:'String',
        value:'closed'
    },
    confirmed_fraud_flag:
    {
        type:'Boolean',
        value:false
    },
    blacklisted_flag:
    {
        type:'String',
        value:'closed'
    },
    blocked_flag:
    {
        type:'String',
        value:'closed'
    },
    vulnerable_flag:
    {
        type:'String',
        value:'closed'
    },
    suspected_fraud_flag:
    {
        type:'String',
        value:'closed'
    },
    suspected_fraud_case:
    {
        type:'String',
        value:'closed'
    },
    financial_difficulties_flag:
    {
        type:'String',
        value:'closed'
    },
    complaint_flag:
    {
        type:'String',
        value:'closed'
    },
    existing_loan:
    {
        type:'Boolean',
        value:true
    },
    outstanding_balance:
    {
        type:'Double',
        value:2
    },
    arrears_balance:
    {
        type:'Double',
        value:1
    },
    loan_expiry_date:
    {
        type:'String',
        value: '2018-11-01 14:14:24'
    },
    last_decision:{
        type:'String',
        value:'accept'
    }
};

const parentModelPayload: TParentModelPayload = {
    withVariablesInReturn: true,
    businessKey: 'corrId',
    variables: camundaNormalizedPayload
};

const createCamundaVariablesEligibilityPayloadMock: VariablesPayload = 
{
        suspended_flag:
        {
            type:'String',
            value:'closed'
        },
        confirmed_fraud_flag:
        {
            type:'Boolean',
            value:false
        },
        blacklisted_flag:
        {
            type:'String',
            value:'closed'
        },
        blocked_flag:
        {
            type:'String',
            value:'closed'
        },
        vulnerable_flag:
        {
            type:'String',
            value:'closed'
        },
        suspected_fraud_flag:
        {
            type:'String',
            value:'closed'
        },
        suspected_fraud_case:
        {
            type:'String',
            value:'closed'
        },
        financial_difficulties_flag:
        {
            type:'String',
            value:'closed'
        },
        complaint_flag:
        {
            type:'String',
            value:'closed'
        },
        existing_loan:
        {
            type:'Boolean',
            value:true
        },
        outstanding_balance:
        {
            type:'Double',
            value:2
        },
        arrears_balance:
        {
            type:'Double',
            value:1
        },
        loan_expiry_date:
        {
            type:'String',
            value: '2019-09-09'
        },
        last_decision:{
            type:'String',
            value:'accept'
        }
};

const data = {
    suspended_flag: 'closed',
    confirmed_fraud_flag: false,
    blacklisted_flag: 'closed',
    blocked_flag: 'closed',
    vulnerable_flag: 'closed',
    suspected_fraud_flag: 'closed',
    suspected_fraud_case: 'closed',
    financial_difficulties_flag: 'closed',
    complaint_flag: 'closed',
    existing_loan: true,
    outstanding_balance: 2,
    arrears_balance: 1,
    loan_expiry_date: '2018-11-01 14:14:24',
    last_decision: 'accept'
};

const camundaResponse: camundaResponse = {
    variables: {
        MODEL_RULES_RESULT_note_eligibility_model: {
            value: '[{ "statusCode": 4 }]'
        }
    }
};

gb.GlobalLogger = {
    info: () => {},
    error: () => {}
};
gb.httpClient = {
    post: () => Promise.resolve({}),
};

db.globalDatabase = {
    query: () => {},
};

let sandbox: any;
describe('eligibility controller', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
	it('happy path', async () => {
        sandbox.stub(camunda, 'createCamundaVariablesEligibilityPayload').callsFake(() => createCamundaVariablesEligibilityPayloadMock);
        sandbox.stub(camunda, 'callEligibilityModel').resolves({
            eligibility: true,
            statusCode: 11,
        });
        const result = await eligibility(req, res);
		expect(result).toEqual({
            eligibility: true,
            statusCode: 11,
        });
    });
});

describe('camunda eligibility', () => {
    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('create camunda payload', async () => {
        const r = await camunda.createCamundaVariablesEligibilityPayload(data);
		expect(r).toEqual(camundaNormalizedPayload);
    });
    it('call eligibility model', async () => {
        let writeEligibilityResultToDB = require('../../dist/database/models/eligibility');
        jest.mock('../../dist/database/models/eligibility');
        writeEligibilityResultToDB = ()  => Promise.resolve({});
        sandbox.stub(camunda, 'normalizeEligibilityCamundaResponse').resolves({
            eligibility: true,
            statusCode: 100,
        });
        const r = await camunda.callEligibilityModel(parentModelPayload, 'corrId', 'clientId', false);
		expect(r).toEqual({
            eligibility: true,
            statusCode: 100,
        });
    });
    it('normalize camunda response', async () => {
        const r = await camunda.normalizeEligibilityCamundaResponse(camundaResponse);
		expect(r).toEqual({
            eligibility: false,
            statusCode: 4,
        });
	});
});

describe('eligibility db', () => {
	it('eligibility result db query', async () => {
        await eligibilityQuery.writeEligibilityResultToDB({
            eligibility: true,
            statusCode: 100
        }, 'corrId', 'clientId');
    });
});


// import { eligibility } from '../../dist/controllers/eligibility';

import { RunnerRequest } from '@myjar/service-runner/dist/Types';
import * as config from '@myjar/config';

import { httpClient, GlobalLogger } from '../core/globals';
import {
	callEligibilityModel,
	createCamundaVariablesEligibilityPayload,
} from '../core/camunda';

export const eligibility = async (req: RunnerRequest, res: Response) => {
	const {
		correlationId,
	} = req;

	const eligibility = config.get('camunda.eligibility.enabled') === '1';

	if (!eligibility) {
		throw new Error('Eligibility process is disabled');
	}
	
	const {
		client_id,
	} = req.body;

	if (!client_id) {
		throw new Error('client_id is required');
	}

    
    // TODO: Implement data fecthing from different sources once known

    // Data sample
    const Customer = {
		suspended_flag: 'closed',
		confirmed_fraud_flag: false,
		blacklisted_flag: 'closed',
		blocked_flag: 'closed',
		vulnerable_flag: 'closed',
		suspected_fraud_flag: 'closed',
		suspected_fraud_case: 'closed',
		financial_difficulties_flag: 'closed',
		complaint_flag: 'closed',
        existing_loan: true,
        outstanding_balance: -2,
        arrears_balance: 1,
		loan_expary_date: '2018-11-01 14:14:24',
		last_decision: 'decline'
    };

	const variables = createCamundaVariablesEligibilityPayload(Customer);

	const parentModelPayload = {
		withVariablesInReturn: true,
		businessKey: correlationId,
		variables
	};

	return callEligibilityModel(parentModelPayload, correlationId as string, client_id, false);
};

// import * as camunda from '../../dist/core/camunda';

export const createCamundaVariablesEligibilityPayload = (customer: GenericObject) => {
	const payload: VariablesPayload = {
        suspended_flag: {
            type: 'String',
            value: customer.suspended_flag
        },
		confirmed_fraud_flag: {
            type: 'Boolean',
            value: customer.confirmed_fraud_flag
        },
		blacklisted_flag: {
            type: 'String',
            value: customer.blacklisted_flag
        },
		blocked_flag: {
            type: 'String',
            value: customer.blocked_flag
        },
		vulnerable_flag: {
            type: 'String',
            value: customer.vulnerable_flag
        },
		suspected_fraud_flag: {
            type: 'String',
            value: customer.suspected_fraud_flag
        },
		suspected_fraud_case: {
            type: 'String',
            value: customer.suspected_fraud_case
        },
		financial_difficulties_flag: {
            type: 'String',
            value: customer.financial_difficulties_flag
        },
		complaint_flag: {
            type: 'String',
            value: customer.complaint_flag
        },
        existing_loan: {
            type: 'Boolean',
            value: customer.existing_loan
        },
        outstanding_balance: {
            type: 'Double',
            value: customer.outstanding_balance
        },
        arrears_balance: {
            type: 'Double',
            value: customer.arrears_balance
        },
        loan_expiry_date: {
            type: 'String',
            value: customer.loan_expiry_date
        },
        last_decision: {
            type: 'String',
            value: customer.last_decision
        }
	};
	
	return payload;
};

export const normalizeEligibilityCamundaResponse = async (camundaResponse: camundaResponse) => {
    if (camundaResponse && camundaResponse.variables && camundaResponse.variables.MODEL_RULES_RESULT_note_eligibility_model && camundaResponse.variables.MODEL_RULES_RESULT_note_eligibility_model.value) {
        const parsedCamundaResponse = JSON.parse(camundaResponse.variables.MODEL_RULES_RESULT_note_eligibility_model.value);
        if (!parsedCamundaResponse.length) {
            return {
                eligibility: true,
                statusCode: 100,
            };
        } else {
            return {
                eligibility: false,
                statusCode: parsedCamundaResponse[0] && parsedCamundaResponse[0].statusCode ? parsedCamundaResponse[0].statusCode : 500,
            };
        }
    } else {
        return {
            eligibility: false,
            statusCode: 500,
        };
    }
}

export const callEligibilityModel = async (parentModelPayload: ParentModelPayload, correlationId: string, client_id: string, shadow: boolean) => {
	const camundaBaseUrl = (shadow) ? config.get('camundaShadow.baseUrl') : config.get('camunda.baseUrl');
    const parentModelKey = (shadow) ? config.get('camundaShadow.eligibility.parentProcessDefinitionKey') : config.get('camunda.eligibility.parentProcessDefinitionKey');
    
    const camundaProcessStartURL = `${camundaBaseUrl}process-definition/key/${parentModelKey}/start`;

    const modelShadowSettings: VariablesPayload = {
        AUTOCAM_SHADOW: {
            type: 'Boolean',
			value: shadow,
        },
    };
    
    GlobalLogger.info('Calling Camunda AutoCAM parent model', {
        shadow,
        camundaProcessStartURL,
        parentModelKey,
        variables: parentModelPayload.variables,
    });

    Object.assign(parentModelPayload.variables, modelShadowSettings);

    const camundaCall = await httpClient.post(camundaProcessStartURL, parentModelPayload).catch(error => {
        GlobalLogger.error('Response from Camunda AutoCAM parent model', {
            shadow,
            camundaProcessStartURL,
            parentModelKey,
            error,
        });
        return error;
    });

    GlobalLogger.info('Response from Camunda AutoCAM parent model', {
        shadow,
        camundaProcessStartURL,
        parentModelKey,
        camundaCall,
    });

    const result = await normalizeEligibilityCamundaResponse(camundaCall);
    await writeEligibilityResultToDB(result, correlationId, client_id);
    return result;
};

// import * as eligibilityQuery from '../../dist/database/models/eligibility';

import { globalDatabase, QueryTypes } from '../connection';
import { GlobalLogger } from '../../core/globals';
import { result } from '../../core/camunda'

export const writeEligibilityResultToDB = async (result: result, correlationId: string, clientId: string) => {
    const decision = result.eligibility;
    const r = JSON.stringify(result);
    const query = 'INSERT INTO public.eligibility (correlation_id, client_id, response, decision) VALUES (:correlationId, :clientId, :r, :decision)';

	try {
		await globalDatabase.query(query, {
			replacements: {
                correlationId,
                clientId,
                r,
                decision,
            },
			type: QueryTypes.SELECT
		});
	} catch (error) {
		GlobalLogger.error(`Error querying last decision: ${error.message}`, error);
		throw error;
    }
}

// const gb = require('../../dist/core/globals');

import Logger = require('@myjar/logger');
import { HttpClient } from '@myjar/http-client';
import ChildLogger = require('@myjar/logger/dist/ChildLogger');

let globalLogger: Logger;
let requestLogger: ChildLogger;
let httpClient: HttpClient;

const setGlobalLogger = (logger: Logger) => {
	globalLogger = logger;
};
const setRequestLogger = (logger: ChildLogger) => {
	requestLogger = logger;
};
const setGlobalHttpClient = (http: HttpClient) => {
	httpClient = http;
};

export {
	globalLogger as GlobalLogger,
	requestLogger as RequestLogger,
	httpClient,
	setGlobalLogger,
	setRequestLogger,
	setGlobalHttpClient,
};
